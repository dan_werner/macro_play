macro_rules! burp {
	( $( $x:expr ),+ ) => {{
			let mut temp = Vec::with_capacity(1);
			$( temp.push($x); )+
			temp
	}}
}

macro_rules! struct_with_delta {
	($s:ident, $( $attr_name:ident :$attr_type:ty ),* ) => (
		// Struct has an internal mirror of all values
		#[derive(Debug)]
		struct Delta {
			$( pub $attr_name : $attr_type ),*
		}
		#[derive(Debug)]
		pub struct $s {
			delta: Delta,
			$( $attr_name : $attr_type ),*
		}
		impl $s {
			fn new( $( $attr_name : $attr_type ),* ) -> Self {
				$s{ 
					delta: Delta {
						$( $attr_name: $attr_name ),*,
					},
					$( $attr_name: $attr_name ),*,
				}
			}
			$(
				#[inline]
				fn $attr_name(&self) -> $attr_type {
					self.$attr_name
				}
			)*
			#[inline]
			fn is_dirty(&self) -> bool {
				$(
					if self.$attr_name == self.delta.$attr_name {
						return true;
					}
				)*
			}
		}
	)
}

#[macro_export]
macro_rules! struct_with_ctor {
	($s:ident, $( $attr_name:ident :$attr_type:ty ),* ) => (
		#[derive(Debug)]
		struct $s {
			$($attr_name : $attr_type),*
		}

		impl $s {
			fn new( $( $attr_name : $attr_type ),* ) -> Self {
				$s{ $( $attr_name: $attr_name ),* }
			}
			$(
				#[inline]
				fn $attr_name(&self) -> $attr_type {
					self.$attr_name
				}
			)*
		}

	)
}

#[macro_export]
macro_rules! struct_with_field_list {
	($s:ident, $( $attr_name:ident :$attr_type:ty ),* ) => (
		struct_with_delta!( $s, $($attr_name : $attr_type),*);
		impl $s {
			fn fields(&self) -> Vec<&'static str> {
				let mut fields = Vec::new();
				$( fields.push( stringify!($attr_name) ); )*
				fields	
			}
		}
	)
}

macro_rules! match_some {
	($e:expr, $p: pat) => {{
		match $e {
			$p => println!("match"),
			_ => println!("Nothing")
		}
	}}
}

macro_rules! scope {
	($b: expr) => { $b }
}

struct_with_field_list!(
	Dollop,
	x: u32,
	y: f32
);

fn main() {
    println!("Hello, world!");
		let temp = "A";
		let merp = burp![temp, "b"];
		let mut g = Dollop::new( 15, 3.0 );
		println!("{:?},  -> {:?}", merp, g);
		println!("{:?}", g.x());
		match_some!(Some(Dollop::new(12, 13.0)), None);

		println!("Dollop fields: {:?}", g.fields()); 

		scope!{{
			let x = 1;
			println!("inside scope {}", x);
		}};
}
